#include <stdlib.h>
#include <stdio.h>
#define EXIT_SUCCESS 0

int postIncrement(int* number){
        int preVal = *number;
        *number = *number + 1;
        return preVal;
}

int preIncrement(int number){
    return number + 1;
}

int main(int argc, char *argv[])
{
    int j;

    for (j = 0; j < argc; ++j)
        printf("argv[%d] = %s\n", j, argv[j]);

    int num = 10;
    int * num2 = &num;
    *num2 = 56;
    int val1 = *num2;
    int val2 = num+val1;
    int value = postIncrement(&val2);
    printf("value: %d \n", value);
    printf("value: %d \n", val2);

    exit(EXIT_SUCCESS);
}