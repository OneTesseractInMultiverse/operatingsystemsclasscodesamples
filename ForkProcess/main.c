#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#define ERROR -1

static int idata = 11;            /* Allocated in data segment */

void runChildLogic(int* initialNumber, int childId){
    for(int i = 0; i < 5; ++i){
        *initialNumber += i;
        sleep(2);
        printf("(child(%d)[%d]): idata: %d \n", childId,  i, idata);
    }
}

void runParentLogic(int* initialNumber, int* factor){
    for(int i = 0; i < 5; ++i){
        *initialNumber *= (*factor + i);
        *factor = *factor - 1;
        sleep(1);
        printf("(parent[%d]): idata: %d | istack: %d \n", i, idata, *factor);
    }
}

int main(int argc, char *argv[])
{
    int istack = 100;              /* Allocated in stack segment */
    pid_t childPid;

    for(int i = 0; i < 12; ++i){
        switch (childPid = fork()) {
            case -1:
                return ERROR;

            case 0:
                runChildLogic(&idata, i);
                break;

            default:
                runParentLogic(&idata, &istack);
                break;
        }
        printf("PID=%ld %s idata=%d istack=%d\n", (long) getpid(),
               (childPid == 0) ? "(child) " : "(parent)", idata, istack);
    }

    /* Both parent and child come here */



    exit(EXIT_SUCCESS);
}