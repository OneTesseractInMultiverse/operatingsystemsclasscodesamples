#include <pthread.h>
#include "tlpi_hdr.h"

static pthread_cond_t threadDied = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t threadMutex = PTHREAD_MUTEX_INITIALIZER;

static int totalThreads = 0;
static int numberOfThreadsAlive = 0;

static int numberOfUnJoinedThreads = 0; // The number of threads that terminated but are not yet joined

enum threadState {
    THREAD_ALIVE,
    THREAD_TERMINATED,
    THREADS_JOINED
};

static struct {
    pthread_t threadValue;
    enum threadState state;
    int sleepTime; // The number of seconds the thread will live before terminating
} *thread;

static void *threadFunction(void *arg) {
    int currentThreadId = (int) arg;
    int statusCode;
    sleep(thread[currentThreadId].sleepTime);  // Here we tell the thread to sleep for "sleepTime" seconds
    printf("Thread %d terminating", currentThreadId);

    statusCode = pthread_mutex_lock(&threadMutex); // Here we close the lock
    if (statusCode != 0)
        errExitEN(statusCode, "pthread_mutex_lock failed");

    numberOfUnJoinedThreads++;
    thread[currentThreadId].state = THREAD_TERMINATED;

    statusCode = pthread_mutex_unlock(&threadMutex); // Here we open the lock
    if (statusCode != 0)
        errExitEN(statusCode, "pthread_mutex_unlock failed");

    statusCode = pthread_cond_signal(&threadDied);
    if (statusCode != 0)
        errExitEN(statusCode, "pthread_cond_signal failed");

    return NULL;
}

int main(int argc, char *argv[]) {

    int statusCode, currentThreadId;

    if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s nsec...\n", argv[0]);

    thread = calloc(argc - 1, sizeof(*thread));
    if (thread == NULL)
        errExit("calloc");

    // Create all the threads that need to run
    for (currentThreadId = 0; currentThreadId < argc - 1; ++currentThreadId) {
        thread[currentThreadId].sleepTime = getInt(argv[currentThreadId + 1], GN_NONNEG, NULL);
        thread[currentThreadId].state = THREAD_ALIVE;
        statusCode = pthread_create(&thread[currentThreadId].threadValue, NULL, threadFunction,
                                    (void *) currentThreadId);
        if (statusCode != 0)
            errExitEN(statusCode, "pthread_create");
    }

    totalThreads = argc - 1;
    numberOfThreadsAlive = totalThreads;

    // Join with terminated threads

    while (numberOfThreadsAlive > 0) {
        statusCode = pthread_mutex_lock(&threadMutex);
        if (statusCode != 0)
            errExitEN(statusCode, "pthread_mutex_lock");

        while (numberOfUnJoinedThreads == 0) {
            statusCode = pthread_cond_wait(&threadDied, &threadMutex);
            if (statusCode != 0) {
                errExitEN(statusCode, "pthread_cond_wait");
            }
        }

        for (currentThreadId = 0; currentThreadId < totalThreads; ++currentThreadId) {
            if (thread[currentThreadId].state == THREAD_TERMINATED) {
                statusCode = pthread_join(thread[currentThreadId].threadValue, NULL);
                if (statusCode != 0)
                    errExitEN(statusCode, "pthread_join");
                thread[currentThreadId].state = THREADS_JOINED;
                numberOfThreadsAlive--;
                numberOfUnJoinedThreads--;
                printf("Thread %d was killed (Alive: %d)\n", currentThreadId, numberOfThreadsAlive);
            }
        }
        statusCode = pthread_mutex_unlock(&threadMutex);
        if (statusCode != 0)
            errExitEN(statusCode, "pthread_mutex_unlock");
    }
    exit(EXIT_SUCCESS);
}